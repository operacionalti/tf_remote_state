resource "aws_s3_bucket" "remote_state" {
  bucket = "${var.prefix}-terraform"

  versioning {
    enabled = true
  }

  tags {
    Name = "${var.prefix}-terraform"
  }
}
