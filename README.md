# Terraform module for storing remote state in an S3 bucket

A simple remote state module for Terraform which creates S3
buckets for environments.

## Usage

```hcl
module "remote_state" {
  source = "gitlab.com:operacionalti/tf_remote_state"
  environment = "stage"

  # optional
  prefix = "acme"
}
```

See `interface.tf` for additional details.

## License

MIT

